<?php

$lang['required']			= "Field %s tidak boleh kosong.";
$lang['isset']				= "Field %s harus diisi.";
$lang['valid_email']		= "Field %s harus berupa alamat email.";
$lang['valid_emails']		= "Field %s harus berupa alamat email.";
$lang['valid_url']			= "Field %s harus berisi URL.";
$lang['valid_ip']			= "Field %s harus berisi IP.";
$lang['min_length']			= "Field %s harus paling tidak berisi %s characters.";
$lang['max_length']			= "Field %s melebihi panjang karakter %s.";
$lang['exact_length']		= "Field %s harus beresi %s karakter.";
$lang['alpha']				= "Field %s harus berisi karakter alphabet.";
$lang['alpha_numeric']		= "Field %s harus berisi karakter alpha-numeric.";
$lang['alpha_dash']			= "Field %s hanya boleh berisi alpha-numeric characters, underscores, and dashes.";
$lang['numeric']			= "Field %s hanya boleh berisi angka.";
$lang['is_numeric']			= "Field %s hanya boleh boleh berisi numerik.";
$lang['integer']			= "Field %s hanya boleh berisi integer.";
$lang['regex_match']		= "Field %s tidak sesuai format.";
$lang['matches']			= "Field %s tidak sesuai dengan %s.";
$lang['is_unique'] 			= "Field %s harus unik.";
$lang['is_natural']			= "Field %s harus berisi angka positif.";
$lang['is_natural_no_zero']	= "Angka %s harus lebih besar 0.";
$lang['decimal']			= "Angka %s harus desimal.";
$lang['less_than']			= "Angka %s harus lebih kecil dari %s.";
$lang['greater_than']		= "Angka %s harus lebih besar dari %s.";


/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */