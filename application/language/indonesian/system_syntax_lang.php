<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// Errors
$lang['error_csrf'] = 'Data tidak lolos chek keamanan.';

// dashboard

$lang['doctor'] = 'Dokter';
$lang['patient'] = 'Pasien';
$lang['nurse'] = 'Perawat';
$lang['pharmacist'] = 'Apoteker';
$lang['laboratorist'] = 'Laboran';
$lang['accountant'] = 'Akuntan';
$lang['payment'] = 'Pembayaran';
$lang['medicine'] = 'Obat';
$lang['report'] = 'Laporan';
$lang['operation_report'] = 'Laporan Operasional';
$lang['birth_report'] = 'Laporan Kelahiran';
$lang['donor'] = 'Donor';
$lang['bed'] = 'Tempat Tidur (Ranap)';
$lang['total_bed'] = 'Jumlah Tempat Tidur (Ranap)';
$lang['expense'] = 'Pengeluaran';
$lang['total_payment'] = 'Total pembayaran';
$lang['departments'] = 'Bagian';
$lang['department'] = 'Bagian';
$lang['human_resources'] = 'Sumberdaya Manusia';
$lang['financial_activities'] = 'Keuangan';
$lang['settings'] = 'Setting';
$lang['profile'] = 'Profil';
$lang['my_profile'] = 'Profile Saya';
$lang['my_consultant'] = 'Konsultan Saya';
$lang['my_due_balance'] = 'Tagihan';
$lang['my_last_login'] = 'Login terakhir';
$lang['my_reports'] = 'Laporan Saya';
$lang['hms'] = 'SIM RSGM'; 

$lang['see_all_patients'] = 'Lihat Semua Pasien'; 
$lang['see_all_payments'] = 'Lihat Semua Pembayaran';
$lang['see_all_medicines'] = 'Lihat Semua Obat';
$lang['payment_today'] = 'Pembayaran Hari Ini';
$lang['payments_today'] = 'Pembayaran Hari Ini';
$lang['patient_registered_today'] = 'Pasien Terdaftar Hari Ini';
$lang['patients_registered_today'] = 'Pasien Terdaftar Hari Ini';
$lang['medicine_registered_today'] = 'Obat Terdaftar Hari Ini';
$lang['medicine_registered_today'] = 'Obat Terdaftar Hari Ini';

$lang['log_out'] = 'Keluar';





//sidebar
$lang['dashboard'] = 'Dashboard';
$lang['all_hospitals'] = 'Keuangan';
$lang['create_new_hospital'] = 'Settings';




// Departments
$lang['add_department'] = 'Tambah Bagian';
$lang['edit_department'] = 'Edit Bagian';
$lang['name'] = 'Nama';
$lang['description'] = 'Deskripsi';
$lang['options'] = 'Opsi';
$lang['print'] = 'Cetak';


// Doctors
$lang['doctors'] = 'Dokter';
$lang['add_doctor'] = 'Tambah Dokter';
$lang['edit_doctor'] = 'Edit Dokter';
$lang['image'] = 'Gambar';
$lang['email'] = 'Email';
$lang['address'] = 'Alamat';
$lang['phone'] = 'Telephon';
$lang['password'] = 'Password';

// Patient
$lang['add_patient'] = 'Tambah Pasien';
$lang['register_new_patient'] = 'Pendaftaran Pasien';
$lang['edit_patient'] = 'Edit Pasien';
$lang['birth_date'] = 'Tanggal Lahir';
$lang['blood_group'] = 'Golongan Darah';
$lang['due_balance'] = 'Total Tagihan';
$lang['invoice'] = 'Faktur/Invoice';
$lang['details'] = 'Detail';
$lang['bio_graph'] = 'Bio Graph';
$lang['patient_id'] = 'ID Pasien';
$lang['gender'] = 'Jenis Kelamin';
$lang['province'] = 'Provinsi';
$lang['regency'] = 'Kabupaten';
$lang['sub_district'] = 'Kecamatan';
$lang['village'] = 'Desa';

// Appoinytment

$lang['appointments'] = 'Janji';
$lang['appointment'] = 'Janji';
$lang['id'] = 'Id';
$lang['date-time'] = 'Tanggal - Waktu';
$lang['remarks'] = 'Catatan';
$lang['add_appointment'] = 'Buat Janji';
$lang['edit_appointment'] = 'Edit Janji';


// Nurse
$lang['add_nurse'] = 'Tambah Perawat';
$lang['edit_nurse'] = 'Edit Perawat';

// Pharmacist
$lang['add_pharmacist'] = 'Tambah Apoteker';
$lang['edit_pharmacist'] = 'Edit Apoteker';

// Laboratorist
$lang['add_laboratorist'] = 'Tambah Laboran';
$lang['edit_laboratorist'] = 'Edit Laboran';

// Accountant
$lang['add_accountant'] = 'Tambah Akuntan';
$lang['edit_accountant'] = 'Edit Tambah';


// Financial Activities
$lang['payments'] = 'Pembayaran';
$lang['date'] = 'Tanggal';
$lang['sub_total'] = 'Sub Total';
$lang['discount'] = 'Diskon';
$lang['vat'] = 'PPN';
$lang['total'] = 'Total';
$lang['status'] = 'Status';
$lang['invoice_info'] = 'INFORMASI FAKTUR';
$lang['invoice_status'] = 'STATUS FAKTUR';
$lang['paid'] = 'Terbayar';

$lang['add_payment'] = 'Add Pembayaran';
$lang['edit_payment'] = 'Edit Pembayaran';
$lang['payment_categories'] = 'Kategori Pembayaran';
$lang['edit_payment_category'] = 'Edit Prosedur Pembayaran';
$lang['amount'] = 'Jumlah';
$lang['submit'] = 'Simpan';
$lang['add_more_categories'] = 'Tambah Kategori';

$lang['category'] = 'Kategori';

$lang['expenses'] = 'Pengeluaran';
$lang['add_expense'] = 'Tambah Pengeluaran';
$lang['edit_expense'] = 'Edit Pengeluaran';

$lang['expense_categories'] = 'Kategori Pengeluaran';
$lang['add_expense_category'] = 'Tambah Kategori Pengeluaran';
$lang['edit_expense_category'] = 'Edit Kategori Pengeluaran';

$lang['financial_report'] = 'Laporan Keuangan';
$lang['payment_report'] = 'Laporan Pembayaran';
$lang['expense_report'] = 'Laporan Pengeluaran';
$lang['gross_payment'] = 'Pembayaran Bersih';
$lang['gross_expense'] = 'Pengeluaran Bersih';
$lang['total'] = 'Total';
$lang['sub_total'] = 'Sub Total';
$lang['profit'] = 'Keuntungan';

$lang['payment_to'] = 'PEMBAYARAN KE';
$lang['bill_to'] = 'TAGIHAN KE';
$lang['invoice_number'] = 'No. Faktur';
$lang['invoice'] = 'Faktur';
$lang['grand_total'] = 'Grand Total';
$lang['make_paid'] = 'Terbayar';
$lang['edit_invoice'] = 'Edit Faktur';

$lang['percentage'] = 'Persentase';
$lang['flat'] = 'Flat';

$lang['payment_procedures'] = 'Prosedur Pembayaran';
$lang['create_payment_procedure'] = 'Buat Prosedur Pembayaran';
$lang['procedures'] = 'Prosedur';
$lang['procedure'] = 'Prosedur'; 




// Medicine
$lang['medicine'] = 'Obat';
$lang['add_medicine'] = 'Tambah Obat';
$lang['medicine_list'] = 'Daftar Obat';
$lang['edit_medicine'] = 'Edit Obat';
$lang['price'] = 'Harga';
$lang['quantity'] = 'Jumlah';
$lang['generic_name'] = 'Nama Generic';
$lang['company'] = 'Pabrik';
$lang['effects'] = 'Efek';
$lang['expiry_date'] = 'Kadaluarsa';

$lang['medicine_category'] = 'Kategori Obat';
$lang['add_medicine_category'] = 'Tambah Kategori';
$lang['edit_medicine_category'] = 'Edit Kategori Obat';

// Donor
$lang['donor'] = 'Donor';
$lang['donor_list'] = 'Daftar Donor';
$lang['add_donor'] = 'Tambah Donor';
$lang['edit_donor'] = 'Edit Donor';
$lang['age'] = 'Umur';
$lang['sex'] = 'Jenis Kelamin';
$lang['last_donation_date'] = 'Tanggal Donasi Terakhir';
$lang['male'] = 'Laki-laki';
$lang['female'] = 'Perempuan';
$lang['blood_bank'] = 'Bank Darah';
$lang['update_blood_bank'] = 'Update Bank Darah';
$lang['group'] = 'Golongan';
$lang['others'] = 'Lainya';


// Bed
$lang['bed'] = 'Tempat Tidur Ranap';
$lang['bed_list'] = 'Daftar Tempat Tidur Ranap';
$lang['add_bed'] = 'Tambah Tempat Tidur Ranap';
$lang['edit_bed'] = 'Edit Tempat Tidur Ranap';
$lang['bed_category'] = 'Kategori Tempat Tidur';
$lang['bed_allotments'] = 'Alokasi Tempat Tidur';
$lang['add_allotment'] = 'Tambah Alokasi';
$lang['edit_allotment'] = 'Edit Alokasi';
$lang['bed_id'] = 'Id Tempat Tidur';
$lang['bed_number'] = 'Nomor Tempat Tidur';
$lang['add_bed_category'] = 'Tambah Kategori Tempat Tidur';
$lang['edit_bed_category'] = 'Edit Kategori Tempat Tidur';
$lang['alloted_time'] = 'Waktu Masuk';
$lang['discharge_time'] = 'Waktu Keluar';
$lang['alloted'] = 'Dipakai';
$lang['available'] = 'Tersedia';
$lang['alloted_beds'] = 'Bed Terpakai';

// All Reports
$lang['e_report'] = 'e_report';
$lang['add_report'] = 'Tambah Laporan';
$lang['add_birth_report'] = 'Tambah Laporan Kelahiran';
$lang['add_operation_report'] = 'Tambah Laporan Operasi';
$lang['add_expire_report'] = 'Tambah Laporan Kadaluarsa';
$lang['edit_birth_report'] = 'Edit Laporan Kelahiran';
$lang['edit_operation_report'] = 'Edit Laporan Operasi';
$lang['edit_expire_report'] = 'Edit Laporan Kadaluarsa';
$lang['edit_report'] = 'Edit Laporan';
$lang['birth'] = 'Kelahiran';
$lang['operation'] = 'Operasi';
$lang['expire'] = 'Kadaluarsa';
$lang['select_type'] = 'Pilih Jenis';
$lang['myreports'] = 'Laporan Saya';
$lang['type'] = 'Jenis';
$lang['expire_report'] = 'Laporan Kadaluarsa';

//Settings
$lang['system_name'] = 'Nama Sistem';
$lang['title'] = 'Judul';
$lang['address'] = 'Alamat';
$lang['phone'] = 'Telp';
$lang['hospital_email'] = 'Email RS';
$lang['currency'] = 'Mata Uang';
$lang['discount_type'] = 'Jenis Diskon';
$lang['registration_fee'] = 'Biaya Pendaftaran';

//Profile
$lang['change_password'] = 'Ubah Password';
$lang['manage_profile'] = 'Atur Profil';

//options button

$lang['edit'] = 'Edit';
$lang['info'] = 'Info';
$lang['delete'] = 'Delete';
$lang['history'] = 'History';



$lang['system_title'] = 'SIM RSGM Soelastri';

$lang['gross_total'] =  'Jumlah Bersih';
$lang['amount_received'] =  'Jumlah Diterima';
$lang['gross_expense'] = 'Pengeluaran Bersih';
$lang['total'] = 'Total';
$lang['sub_total'] = 'Sub Total';
$lang['due_amount'] = 'Jumlah Tagihan';
$lang['due_balance'] = 'Saldo';
$lang['patient_id'] = 'ID Pasien';

$lang['unpaid'] = 'Belum Terbayar';
$lang['paid_partially'] = 'Terbayar Sebagian';
$lang['paid'] = 'Terbayar';

$lang['amount_to_be_paid'] = 'Jumlah Harus Dibayar';
$lang['amount_received'] = 'Jumlah Diterima';

$lang['confirmation'] = 'Konfirmasi';

$lang['payment_to'] = 'Pembayaran Kepada';

$lang['bill_to'] = 'Tagihan Kepada';

$lang['patient'] = 'Pasien';

$lang['refd_by_doctor'] = 'Refd oleh Dokter';

$lang['date'] = 'Tanggal';

$lang['sub_total'] = 'Sub Total';

$lang['discount'] = 'Diskon';

$lang['doctor_amount'] = 'Jumlah Dokter';

$lang['hospitl_amount'] = 'Jumlah RS';




$lang['all'] = 'Semua';
$lang['today'] = 'Hari ini';
$lang['yesterday'] = 'Kemarin';
$lang['all_bills'] = 'Semua Tagihan';
$lang['date'] = 'Tanggal';
$lang['bill_amount'] = ' Jumlah Tagihan';
$lang['deposits'] = 'Deposit';
$lang['deposit'] = 'Deposit';
$lang['hospitl_amount'] = 'Jumlah RS';
$lang['hospitl_amount'] = 'Jumlah RS';

$lang['hospital_management_system'] = 'SIM RSGM Soelastri';
$lang['user_activity_report'] = 'Aktifitas Pengguna';

$lang['list_of_doctors'] = 'Daftar Dokter';
$lang['doctors_commission'] = 'Komisi Dokter';

$lang['commission'] = 'Komisi';
$lang['surgeon_fee'] = 'Fee Operasi';

$lang['ot_payments'] = 'Pembayaran Lembur';
$lang['add_ot_payment'] = 'Tambah Pembayaran Lembur';
$lang['payment_procedures'] = 'Prosedur Pambayaran';

$lang['activities_by'] = 'Aktifitas oleh';
$lang['all_users'] = 'Semua Pengguna';
$lang['deposited_amount'] = 'Jumlah Deposit';
$lang['payment_received'] = 'Pembayaran Diterima';


$lang['payment_history'] = 'Riwayat Pembayaran';

$lang['add_new'] = 'Tambah Baru';
$lang['add_new_doctor'] = 'Tambah Dokter Baru';

$lang['add_general_payment'] = 'Tambah Pembayaran Umum';
$lang['choose_payment_type'] = 'Pilih Jenis Pembayaran';

$lang['total_bill_amount'] = 'Total Tagihan';
$lang['total_deposit_amount'] = 'Total Deposit';

$lang['add_another_ot_payment'] = 'Tambah Pembayaran Lembur';

$lang['consultant_surgeon'] = 'Konsultan Bedah';
$lang['assistant_surgeon'] = 'Asisten Bedah';
$lang['anaestheasist'] = 'Ahli Anestesi';

$lang['nature_of_operation'] = 'Kondisi Operasi';


$lang['ot_charge'] = 'Cas Lembur';
$lang['cabin_rent'] = 'Sewa Ruang';     
$lang['seat_rent'] = 'Sewa Kursi';

$lang['rate'] = 'Tarif';

$lang['medicine_categories'] = 'Kategori Obat';
$lang['create_medicine_category'] = 'Buat Kategori Obat';

$lang['bed_categories'] = 'Kategori Tempat Tidur';
$lang['add_new_allotment'] = 'Tambah Alokasi Tempat Tidur';

$lang['birth_report'] = 'Laporan Kelahiran';
$lang['add_new_report'] = 'Tambah Laporan';

$lang['add_new_patient'] = 'Tambah Pasien Baru';
$lang['add_new_medicine'] = 'Tambah Obat Baru';
$lang['add_new_doctor'] = 'Tambah Dokter Baru';
$lang['add_new_laboratorist'] = 'Tambah Laboran Baru';
$lang['add_new_donor'] = 'Tambah Donor Baru';




$lang['add_new_payment'] = 'Tambah Pembayaran';
$lang['edit_ot_payment'] = 'Edit Pembayaran Lembur';




$lang['add_new_pharmacist'] = 'Tambah Apoteker';

$lang['add_deposit'] = 'Tambah Deposit';
$lang['edit_deposit'] = 'Edit Deposit';
$lang['deposit_amount'] = 'Jumlah Deposit';

$lang['add_accountant'] = 'Tambah Akuntan';

$lang['see_all_donors'] = 'Lihat Semua Donor';
$lang['see_all_reports'] = 'Lihat Semua Laporan';




$lang['add_another_payment'] = 'Tambah Pembayaran';

$lang['receptionist'] = 'Resepsionis';
$lang['add_receptionist'] = 'Tambah Resepsionis';
$lang['edit_receptionist'] = 'Edit Resepsionis';
$lang['all_receptionist'] = 'Semua Resepsionis';
$lang['receptionists'] = 'Resepsionis';

$lang['my_report'] = 'Laporan Saya';

$lang['medical_history'] = 'Riwayat Medis';
$lang['add_medical_history'] = 'Tambah Riwayat Medis';
$lang['edit_medical_history'] = 'Edit Riwayat Medis';



$lang['add'] = 'Tambah';
$lang['material'] = 'Materi';
$lang['download'] = 'Download';


$lang['file'] = 'File';
$lang['files'] = 'File';

//resep
$lang['prescription'] = 'Resep';

$lang['view'] = 'Lihat';
$lang['note'] = 'Catatan';
$lang['medication'] = 'Obat';
$lang['add_prescription'] = 'Tambah Resep';
$lang['edit_prescription'] = 'Edit Resep';

$lang['list_of_departments'] = 'Daftar Bagian';
$lang['gross_income'] = 'Pendapatan Bersih';
$lang['hospital_amount'] = 'Jumlah RS';
$lang['doctors_amount'] = 'Jumlah Dokter';
$lang['ot_income_report'] = 'Laporan Pendapatan Lembur';

$lang['ot_payment'] = 'Pembayaran Lembur';
$lang['ot_discount'] = 'Diskon Lembur';
$lang['gross_ot_income'] = 'Pendapatan Bersih Lembur';

$lang['discount_type'] = 'Jenis Diskon';
$lang['gross_bill'] = 'Tagihan Bersih';

$lang['gross_hospital_amount'] = 'Jumlah RS'; 
$lang['gross_doctors_commission'] = 'Komisi Bersih Dokter';
$lang['gross_deposit'] = 'Deposit Bersih';
$lang['gross_due'] = 'Jumlah Jatuh Tempo';
$lang['gross_expense'] = 'Pengeluaran Bersih';

$lang['date_from'] = 'Dari Tanggal';
$lang['date_to'] = 's/d';
$lang['add_new_medicine_category'] = 'Tambah Kategori Obat';
$lang['edit_medicine_category'] = 'Edit Kategori Obat';
$lang['gross_bill'] = 'Tagihan Bersih';




$lang['add_bed_allotment'] = 'Tambah Alokasi Tempat Tidur';
$lang['edit_bed_aalotment'] = 'Edit Alokasi Tempat Tidur';

$lang['add_a_allotment'] = 'Tambah Penggunaan';
$lang['no_bed_is_available_for_allotment'] = 'Tidak Tersedia Tempat Tidur';
$lang['payment_today'] = 'Pembayaran Hari Ini';
$lang['payments_today'] = 'Pembayaran Hari Ini';
$lang['patient_registerred_today'] = 'Pasien Terdaftar Hari Ini';
$lang['patients_registerred_today'] = 'Pasien Terdaftar Hari Ini';
$lang['donor_registerred_today'] = 'Donor Terdaftar Hari Ini';
$lang['donors_registerred_today'] = 'Donor Terdaftar Hari Ini';
$lang['medicine_registerred_today'] = 'Obat Terdaftar Hari Ini';
$lang['medicines_registered_today'] = 'Obat Terdaftar Hari Ini';




$lang['report_added_today'] = 'Laporan Ditambahkan Hari Ini';
$lang['reports_added_today'] = 'Laporan Ditambahkan Hari Ini';
$lang['report_is_available_for_you'] = 'Laporan Tersedia untuk Anda';
$lang['reports_are_available_for_you'] = 'Laporan Tersedia untuk Anda';
$lang['bed_is_available'] = 'Tempat Tidur Tersedia';

$lang['beds_are_available'] = 'Tempat Tidur Tersedia';
$lang['gross_bill'] = 'Tagihan Bersih';
//$lang['gross_bill'] = 'Gross Bill';
//$lang['gross_bill'] = 'Gross Bill';
//$lang['gross_bill'] = 'Gross Bill';


// For Update

$lang['amount_distribution'] = 'Jlm Distribusi Dokter dan RS';
$lang['invoice_id'] = 'ID Faktur';

$lang['start_time'] = 'Waktu Mulai';
$lang['end_time'] = 'Waktu Berakhir';

$lang['language'] = 'Bahasa';
$lang['english'] = 'English';
$lang['spanish'] = 'Spanish';
$lang['french'] = 'French';
$lang['italian'] = 'Italian';
$lang['portuguese'] = 'Portuguese';

$lang['system_settings'] = 'Setting Sistem';
$lang['backup_database'] = 'Backup Database';

$lang['restore'] = 'Restore Database';

$lang['select'] = 'Pilih';

$lang['database'] = 'Database';

$lang['calendar'] = 'Calendar';



 
$lang['income'] = 'Pendapatan';

$lang['from'] = 'dari';

$lang['to'] = 's/d';


$lang['due'] = 'Jatuh Tempo';

$lang['case_history'] = 'Riwayat Kasus';


$lang['patient_list'] = 'Daftar Pasien';

$lang['documents'] = 'Dokumen';


$lang['case'] = 'Kasus';

$lang['history'] = 'Riwayat';

$lang['document'] = 'Dokumen';


$lang['send_sms'] = 'Kirim WA';
$lang['list_of_sent_messages'] = 'Daftar Pesan Terkirim';
$lang['send_sms_to'] = 'Kirim WA Ke';

$lang['all_patient'] = 'Semua Pasien';
$lang['all_doctor'] = 'Semua Dokter';

$lang['blood_group_wise'] = 'Golongan Darah';
$lang['select_blood_group'] = 'Pilih Golongan Darah';

$lang['message'] = 'Pesan';

$lang['single_patient'] = 'Pasien';
$lang['select_patient'] = 'Pilih Pasien';
$lang['staff'] = 'Staff';

$lang['select_staff'] = 'Pilih Staff';

$lang['sms'] = 'Whatsapp';
$lang['sms_settings'] = 'Setting Whatsapp';


$lang['send_email'] = 'Kirim Email';
$lang['send_email_to'] = 'Kirim Email Kepada';
$lang['email'] = 'Email';
$lang['email_settings'] = 'Setting Email';
$lang['send_email_to_patient'] = 'Kirim Email ke Pasien';
$lang['sent'] = 'Terkirim';
$lang['new'] = 'Baru';



$lang['doctor_id'] = 'Id Dokter';

$lang['diagnostic_test'] = 'Diagnosis';

$lang['report_result'] = 'Laporan Hasil';

$lang['diagnostic_result'] = 'Hasil Diagnosis';

$lang['diagnostic_report'] = 'Laporan Diagnosis';

$lang['diagnostic'] = 'Diagnosis';

$lang['value'] = 'Nilai';

$lang['diagnostic_test_result'] = 'Hasil Test Diagnosis';

$lang['add_update'] = 'Tambah / Ubah';

$lang['doctors_commission'] = 'Komisi Dokter';

$lang['total_doctors_commission'] = 'Total Komisi Dokter';


$lang['treatment_history'] = 'Riwayat Tindakan';

$lang['number_of_patient_treated'] = 'Jumlah Pasien Diselesaikan';




$lang['all_appointments'] = 'Semua Janji';

$lang['todays_appointments'] = 'Janji Hari Ini';


$lang['all'] = 'Semua';
$lang['add'] = 'Tambah';
$lang['todays'] = 'Hari Ini';
$lang['upcoming'] = 'Yang Akan Datang';




$lang['write_message'] = 'Tulis Pesan';
$lang['sent_messages'] = 'Pesan Terkirim';

$lang['send_sms_to_patient'] = 'Kirim WA ke Pasien';


$lang['reminder_message'] = 'Apakah anda akan mengirim pengingat janji?';




$lang['yes'] = 'Ya';
$lang['cancel'] = 'Batal';




$lang['recipient'] = 'Penerima';
$lang['api'] = 'API';
$lang['password'] = 'Password';



$lang['pharmacy'] = 'Farmasi';
$lang['store_box'] = 'Gudang';
$lang['all_sales'] = 'Semua Penjualan';
$lang['poss'] = 'Sistem Penjualan';
$lang['sales'] = 'Penjualan';
$lang['add_new_sale'] = 'Tambah Penjualan Baru';
$lang['load'] = 'Muat';
$lang['p_price'] = 'Harga Beli';
$lang['s_price'] = 'Harga Jual';
$lang['purchase'] = 'Pembelian';
$lang['sale'] = 'Penjualan';
$lang['add_sale'] = 'Tambah Penjualan';
$lang['item_name'] = 'Nama Item';
$lang['total'] = 'Total';
$lang['cost'] = 'Biaya';


$lang['unit_price'] = 'Harga Satuan';
$lang['total_per_item'] = 'Total Per Item';
$lang['medicine_stock_alert'] = 'Pengingat Stok Obat';
$lang['alert_stock_list'] = 'Pengingat Daftar Stok';

$lang['login_title'] = 'Judul Login';
$lang['login_logoo'] = 'Logo Login';
$lang['invoice_logo'] = 'Logo Faktur';
$lang['logo'] = 'Logo';

$lang['load_medicine'] = 'Load Obat';
$lang['add_quantity'] = 'Tambah Kuantitas';

$lang['select_item'] = 'Pilih Item';
$lang['invoice_logo'] = 'Logo Faktur';

$lang['today_sales'] = ' Penjualan Hari Ini';
$lang['today_expense'] = 'Pengeluaran Hari Ini';
$lang['sales_graph'] = 'Grafik Penjualan';
$lang['statistics'] = 'Statistik';
$lang['this_month'] = 'Bulan Ini';
$lang['number_of_sales'] = 'Jumlah Penjualan';
$lang['total_sales'] = 'Total Penjualan';
$lang['number_of_expenses'] = 'Number Of Expenses';
$lang['total_expense'] = 'Total Expense';
$lang['medicine_number'] = 'Nomor Obat';
$lang['medicine_quantity'] = 'Kuantitas Obat';
$lang['medicine_o_s'] = 'Obat Stok Habis';
$lang['latest_sales'] = 'Penjualan Terakhir';
$lang['latest_expense'] = 'Pengeluaran Terakhir';
$lang['latest_medicines'] = 'Obat Terakhir';
$lang['pharmacy_invoices'] = 'Faktur Apotik';
$lang['add_sale'] = 'Tambah Penjualan';


$lang['arabic'] = 'Bahasa Arab';


$lang['payment_gateways'] = 'Jalur Pembayaran';
$lang['labs'] = 'Tes Lab';
$lang['add_lab'] = 'Tambah Tes Lab';
$lang['add_new_lab'] = 'Tambah Tes Lab Baru';
$lang['edit_lab'] = 'Edit Tes Lab';
$lang['lab_category'] = 'Kategori Lab';
$lang['add_lab_category'] = 'Tambah Kategori Lab';
$lang['reference'] = 'Referensi';
$lang['lab_reports'] = 'Laporan Lab';
$lang['add_lab_report'] = 'Tambah Laporan Lab';
$lang['lab_tests'] = 'Tes Lab';
$lang['add_lab_test'] = 'Tambah Tes Lab';
$lang['test_name'] = 'Test Name';
$lang['lab_report'] = 'Laporan Lab';
$lang['reference_value'] = 'Nilai Referensi';
$lang['edit_lab_test'] = 'Edit Tes Lab';
$lang['report_id'] = 'ID Laporan';
$lang['result'] = 'Hasil';
$lang['add_a_new_report'] = 'Tambah Laporan Baru';
$lang['lab_procedures'] = 'Prosedur Lab';
$lang['edit_lab_report'] = 'Edit Laporan Lab';
$lang['change_language'] = 'Ubah Bahasa';
$lang['add_documents'] = 'Tambah Dokumen';
$lang['change_settings'] = 'Ubah Setting';


$lang['quick_links'] = 'Tautan';
$lang['expense_graph'] = 'Grafik Pengeluaran';
$lang['patient_appointment_graph'] = 'Gafik Janji dengan Pasien';
$lang['this_month_sale_expense_pie_chart'] = 'Pie Chart Penjualan - Pengeluaran Bulan Ini';




$lang['lab'] = 'Laboratorium';


$lang['template'] = 'Template';

$lang['add_template'] = 'Tambah Template';

$lang['dosage'] = 'Dosis';
$lang['frequency'] = 'Frekuensi';
$lang['days'] = 'Hari';
$lang['instruction'] = 'Petunjuk';
$lang['symptom'] = 'Gejala';

$lang['website'] = 'Website';
$lang['website_settings'] = 'Setting Website';
$lang['slides'] = 'Slides';
$lang['services'] = 'Layanan';
$lang['featured_doctors'] = 'Daftar Dokter';


$lang['slide'] = 'Slide';
$lang['add_slide'] = 'Tambah Slide';
$lang['edit_slide'] = 'Edit Slide';
$lang['text1'] = 'Text 1';
$lang['text2'] = 'Text 2';
$lang['text3'] = 'Text 3';
$lang['position'] = 'Posisi';
$lang['Status'] = 'Status';

$lang['service'] = 'Layanan';
$lang['add_service'] = 'Add Layanan';
$lang['edit_service'] = 'Edit Layanan';

$lang['featured_doctors'] = 'Dokter Utama';
$lang['add_doctor'] = 'Tambah Dokter';




$lang['emergency'] = 'Kedaruratan';
$lang['support_number'] = 'Nomor Bantuan';
$lang['block_1_text_under_title'] = 'Sub Title';
$lang['service_block__text_under_title'] = 'Blok Layanan';
$lang['doctor_block__text_under_title'] = 'Info Dokter';
$lang['facebook_id'] = 'ID Facebook';
$lang['twitter_id'] = 'ID Twitter';
$lang['google_id'] = 'ID Google';
$lang['youtube_id'] = 'ID Youtube';
$lang['skype_id'] = 'ID Skype';

$lang['visit_site'] = 'Kunjungi Situs';

$lang['activity'] = 'Aktifitas';


$lang['timeline'] = 'Timeline';



$lang['user'] = 'Pengguna';


$lang['holiday'] = 'Hari Libur';

$lang['holidays'] = 'Hari Libur';

$lang['weekday'] = 'Hari Kerja';

$lang['friday'] = 'Jumat';
$lang['saturday'] = 'Sabtu';
$lang['sunday'] = 'Minggu';
$lang['monday'] = 'Senin';
$lang['tuesday'] = 'Selasa';
$lang['wednesday'] = 'Rabu';
$lang['thursday'] = 'Kamis';

$lang['time_schedule'] = 'Jadwal';

$lang['schedule'] = 'Jadwal';

$lang['duration'] = 'Durasi';


$lang['minitues'] = 'Menit';

$lang['work_place'] = 'Tempat Kerja';

$lang['patient_deleted'] = 'Pasien Dihapus';


$lang['time_slot'] = 'Slot Waktu';

$lang['request_a_appointment'] = 'Ajukan Janji';


$lang['add_new_bed'] = 'Tambah Tempat Tidur Baru';


$lang['notice'] = 'Pemberitahuan';
$lang['add_notice'] = 'Tambah Pemberitahuan';
$lang['edit_notice'] = 'Edit Pemberitahuan';
$lang['start'] = 'Mulai';
$lang['end'] = 'Akhir';

$lang['notice_for'] = 'Pemberitahuan untuk';

$lang['available_slots'] = 'Slot Tersedia';

$lang['request'] = 'Permintaan';

$lang['requests'] = 'Permintaan';

$lang['approved'] = 'Disetujui';
$lang['done'] = 'Selesai';
$lang['pending'] = 'Pending';

$lang['requested'] = 'Requested';
$lang['pending_confirmation'] = 'Konfirmasi Pending';
$lang['confirmed'] = 'Terkonfirmasi';
$lang['treated'] = 'Ditangani';
$lang['cancelled'] = 'Dibatalkan';

$lang['items'] = 'Item';
$lang['qty'] = 'Qty';

$lang['save'] = 'Simpan';


$lang['signature'] = 'Tanda Tangan';


$lang['deposit_type'] = 'Tipe Deposit'; 

$lang['payment_gateway'] = 'Jalur Pembayaran'; 

$lang['manage'] = 'Atur'; 

$lang['live'] = 'Live'; 

$lang['test'] = 'Tes'; 

$lang['merchant_key'] = 'Kunci Pelapak';
$lang['salt'] = 'Garam';

$lang['checkout'] = 'Selesai Belanja';


$lang['total_payable_amount'] = 'Total Tagihan';
$lang['checkout_confirmation'] = 'Konfirmasi';
$lang['payment_id'] = 'ID Pembayaran';


$lang['card'] = 'Kartu';
$lang['cvv'] = 'CVV';
$lang['number'] = 'Nomor';


$lang['mastercard'] = 'Master Card';
$lang['visa'] = 'Visa';
$lang['american_express'] = 'American Express'; 

$lang['accepted'] = 'Diterima'; 
$lang['cards'] = 'Kartu'; 

$lang['paypal'] = 'PayPal'; 
$lang['payu'] = 'Pay U Money'; 

$lang['cash'] = 'Cash'; 

$lang['api_username'] = 'API Username'; 
$lang['api_password'] = 'API Password'; 
$lang['api_signature'] = 'API Signature'; 

$lang['back_to_payment_modules'] = 'Kembali ke Modul Pembayaran'; 


$lang['manager'] = 'Pengelola'; 
$lang['cases'] = 'Kasus'; 




$lang['add_case'] = 'Tambah Kasus';
$lang['edit_case'] = 'Edit Kasus';

$lang['subject'] = 'Subject';


$lang['admin'] = 'Admin';


$lang['hospital_statistics'] = 'Statistik Rumah Sakit';

$lang['hospital'] = 'Hospital';

$lang['summary'] = 'Summary';

$lang['per_month_income_expense'] = 'Pendapatan / Pengeluaran per Bulan';
$lang['months'] = 'Bulan';


$lang['creation'] = 'Pembuatan';

$lang['back_to_lab_module'] = 'Kembali ke Modul Lab';

$lang['recommended_size'] = 'Ukuran Direkomendasikan';

$lang['email_settings_instruction_1'] = 'Harus alamat email. Misalnya hostnya ada lah rsgmsoelastri.com.';

$lang['email_settings_instruction_2'] = 'Maka emailnya adalah something@rsgmsoelastri.com untuk pengiriman'; 

$lang['active'] = 'Aktif'; 

$lang['in_active'] = 'Tidak Aktif'; 



$lang['home'] = 'Home'; 

$lang['booking'] = 'Booking'; 

$lang['contact'] = 'Kontak'; 

$lang['authkey'] = 'Auth Key'; 

$lang['sender'] = 'Pengirim';  

$lang['sms_gateway'] = 'WA Gateway'; 
$lang['sms_gateways'] = 'WA Gateways';  


$lang['print_label'] = 'Cetak Label';

$lang['manaj_dental_unit'] = 'Pengelolaan Dental Unit';

$lang['e_mr'] = 'Electronic Medical Record';

$lang['print_card'] = 'Kartu';

$lang['print_barcode'] = 'Barcode';
$lang['print_regristration'] = 'Pendaftaran';


$lang['no_rm'] = 'No. Rekam Medis';
$lang['place_of_birth'] = 'Tempat Lahir';
$lang['nik'] = 'No Induk Kependudukan';
$lang['nationality'] = 'Kewarganegaraan';
$lang['pendidikan'] = 'Pendidikan Terakhir';
$lang['religion'] = 'Agama';
$lang['profession'] = 'Pekerjaan';
$lang['status'] = 'Status';
$lang['kel_pasien'] = 'Kelompok Pasien';
$lang['pembayaran'] = 'Cara Pembayaran';
$lang['pnj'] = 'Nama Penanggung Jawab';
$lang['pnj_tlp'] = 'No. Telepon / Hp';


$lang['dentchart'] = 'Dental Chart';











