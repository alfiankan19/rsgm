<!--sidebar end-->
<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <section class="">

            <header class="panel-heading">
                <?php echo lang('patient'); ?> <?php echo lang('database'); ?>
                <div class="col-md-4 no-print pull-right"> 
                    <a data-toggle="modal" href="#myModal">
                        <div class="btn-group pull-right">
                            <button id="" class="btn green btn-xs">
                                <i class="fa fa-plus-circle"></i> <?php echo lang('add_new'); ?>
                            </button>
                        </div>
                    </a>
                </div>
            </header>
            <div class="panel-body">

                <div class="adv-table editable-table ">

                    <div class="space15"></div>
                    <table class="table table-striped table-hover table-bordered" id="editable-sample">
                        <thead>
                            <tr>
                                <th><?php echo lang('patient_id'); ?></th>                        
                                <th><?php echo lang('name'); ?></th>
                                <th><?php echo lang('phone'); ?></th>
                                <?php if ($this->ion_auth->in_group(array('admin', 'Accountant', 'Receptionist'))) { ?>
                                    <th><?php echo lang('due_balance'); ?></th>
                                <?php } ?>
                                <th class="no-print"><?php echo lang('options'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                        <style>
                            .img_url{
                                height:20px;
                                width:20px;
                                background-size: contain; 
                                max-height:20px;
                                border-radius: 100px;
                            }
                        </style>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
<!--footer start-->
 

<!-- Add Patient Modal-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">  <?php echo lang('register_new_patient'); ?></h4>
            </div>
            <div class="modal-body row">
                <form role="form" action="patient/addNew" class="clearfix" method="post" enctype="multipart/form-data">

                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('no_rm'); ?></label>
                        <input type="text" class="form-control" name="no_rm" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('name'); ?></label>
                        <input type="text" class="form-control" name="name" id="exampleInputEmail1" value='' placeholder="">
                    </div>

                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('nik'); ?></label>
                        <input type="text" class="form-control" name="nik" id="exampleInputEmail1" value='' placeholder="">
                    </div>

                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('email'); ?></label>
                        <input type="text" class="form-control" name="email" id="exampleInputEmail1" value='' placeholder="">
                    </div>

                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('password'); ?></label>
                        <input type="password" class="form-control" name="password" id="exampleInputEmail1" placeholder="">
                    </div>

                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('phone'); ?></label>
                        <input type="text" class="form-control" name="phone" id="exampleInputEmail1" value='' placeholder="">
                    </div>

                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('place_of_birth'); ?></label>
                        <input type="text" class="form-control" name="placeofbirth" id="exampleInputEmail1" value='' placeholder="">
                    </div>

                    <div class="form-group col-md-6">
                        <label><?php echo lang('birth_date'); ?></label>
                        <input class="form-control form-control-inline input-medium default-date-picker" type="text" name="birthdate" value="" placeholder="" readonly="">      
                    </div>

                     <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('sex'); ?></label>
                        <select class="form-control m-bot15" name="sex" value=''>

                            <option value="Laki - Laki" <?php
                            if (!empty($patient->sex)) {
                                if ($patient->sex == 'Laki - Laki') {
                                    echo 'selected';
                                }
                            }
                            ?> > Laki - Laki </option>
                            <option value="Perempuan" <?php
                            if (!empty($patient->sex)) {
                                if ($patient->sex == 'Perempuan') {
                                    echo 'selected';
                                }
                            }
                            ?> > Perempuan </option>
                            <option value="Lainnya" <?php
                            if (!empty($patient->sex)) {
                                if ($patient->sex == 'Lainnya') {
                                    echo 'selected';
                                }
                            }
                            ?> > Lainnya </option>
                        </select>
                    </div>

                     <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('blood_group'); ?></label>
                        <select class="form-control m-bot15" name="bloodgroup" value=''>
                            <?php foreach ($groups as $group) { ?>
                                <option value="<?php echo $group->group; ?>" <?php
                                if (!empty($patient->bloodgroup)) {
                                    if ($group->group == $patient->bloodgroup) {
                                        echo 'selected';
                                    }
                                }
                                ?> > <?php echo $group->group; ?> </option>
                                    <?php } ?> 
                        </select>
                    </div>

                     <div class="form-group col-md-6">
                            <label for="exampleInputEmail1"><?php echo lang('pendidikan') ?></label>
                            <select class="form-control m-bot15" name="lasteducation" value='' placeholder="">
                                <option value="SD">SD</option>
                                <option value="SMP">SMP</option> 
                                <option value="SMA">SMA</option> 
                                <option value="DIPLOMA">Diploma</option>  
                                <option value="S1">S1</option> 
                                <option value="S2">S2</option> 
                                <option value="S3">S3</option> 
                            </select>
                        </div>

                    <div class="form-group col-md-6">
                            <label for="exampleInputEmail1"><?php echo lang('nationality') ?></label>
                            <select class="form-control m-bot15" name="nationality" value='' placeholder="">
                                <option value="WNI">WNI</option>
                                <option value="WNA">WNA</option> 
                            </select>
                        </div>
                    
                    <div class="form-group col-md-6">
                            <label for="exampleInputEmail1"><?php echo lang('religion') ?></label>
                            <select class="form-control m-bot15" name="religion" value='' placeholder="">
                                <option value="Islam">Islam</option>
                                <option value="Kristen">Kristen</option> 
                                <option value="Katholik">Katholik</option> 
                                <option value="Hindu">Hindu</option>  
                                <option value="Budha">Budha</option> 
                                <option value="Lain - Lain">Lain - Lain</option> 
                            </select>
                        </div>
                
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('address'); ?></label>
                        <input type="text" class="form-control" name="address" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    
                    
                     <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('province') ?></label>
                        <select class="form-control m-bot15" name="province" value='' placeholder="" onchange="getKabupaten(this.value)">
                            <?php foreach ($province as $p) { ?>
                                <option value="<?php echo $p->id; ?>" <?php
                                if (!empty($detailaddress->kpu_idprov)) {
                                    if ($p->id == $detailaddress->kpu_idprov) {
                                        echo 'selected';
                                    }
                                }
                                ?>> <?php echo $p->provinsi; ?> </option>
                                    <?php } ?>
                        </select>
                    </div>

                    <div id="kab">
                        <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('regency') ?></label>
                        <select class="form-control m-bot15" name="regency" value='' placeholder="" onchange="getKecamatan(this.value)">
                            <?php foreach ($regency as $r) { ?>
                                <option value="<?php echo $r->id; ?>" <?php
                                if (!empty($detailaddress->kpu_idkab)) {
                                    if ($r->id == $detailaddress->kpu_idkab) {
                                        echo 'selected';
                                    }
                                }
                                ?>> <?php echo $r->kabkot; ?> </option>
                                    <?php } ?>
                        </select>
                    </div>
                    </div> 

                    <div id="kec">
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1"><?php echo lang('sub_district') ?></label>
                            <select class="form-control m-bot15" name="sub_district" value='' placeholder="" onchange="getDesa(this.value)">
                                <?php foreach ($sub_district as $s) { ?>
                                    <option value="<?php echo $s->id; ?>" <?php
                                    if (!empty($detailaddress->kpu_idkec)) {
                                        if ($s->id == $detailaddress->kpu_idkec) {
                                            echo 'selected';
                                        }
                                    }
                                    ?>> <?php echo $s->kecamatan; ?> </option>
                                        <?php } ?>
                            </select>
                        </div>
                    </div> 

                     <div id="desa">
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1"><?php echo lang('village') ?></label>
                            <select class="form-control m-bot15" id="village" name="village" value='' placeholder="">
                                
                                <?php if(isset($village)){
                                    foreach ($village as $v) { ?>                                        
                                        <option value="<?php echo $v->id; ?>"
                                        <?php
                                        if (!empty($patient->id_desa)) {
                                            if ($v->id_desa == $patient->id_desa) {
                                                echo 'selected';
                                            }
                                        }?>>
                                            <?php echo $v->desa; ?> </option>
                                <?php } 
                                } ?>   
                            </select>
                        </div>
                    </div> 

                    
                    
                    <div class="form-group col-md-6">
                            <label for="exampleInputEmail1"><?php echo lang('profession') ?></label>
                            <select class="form-control m-bot15" name="profession" value='' placeholder="">
                                <option value="PNS">PNS</option>
                                <option value="TNI/POLRI">TNI/POLRI</option> 
                                <option value="Wiraswasta">Wiraswasta</option> 
                                <option value="Pelajar/Mahasiswa">Pelajar/Mahasiswa</option>  
                                <option value="Swasta">Swasta</option> 
                                <option value="Lain - Lain">Lain - Lain</option> 
                            </select>
                        </div>

                    
                    <div class="form-group col-md-6">
                            <label for="exampleInputEmail1"><?php echo lang('status') ?></label>
                            <select class="form-control m-bot15" name="status" value='' placeholder="">
                                <option value="Belum Menikah">Belum Menikah</option>
                                <option value="Menikah">Menikah</option> 
                                <option value="Janda">Janda</option> 
                                <option value="Duda">Duda</option>  
                            </select>
                        </div>

                    <div class="form-group col-md-6">
                            <label for="exampleInputEmail1"><?php echo lang('kel_pasien') ?></label>
                            <select class="form-control m-bot15" name="kel_pasien" value='' placeholder="">
                                <option value="Umum">Umum</option>
                                <option value="Mahasiswa UMS">Mahasiswa UMS</option> 
                                <option value="Karyawan/Dosen/Rektor UMS">Karyawan/Dosen/Rektor UMS</option> 
                                <option value="Karyawan RSGM Soelastri">Karyawan RSGM Soelastri</option>  
                            </select>
                        </div>

                    <div class="form-group col-md-6">
                            <label for="exampleInputEmail1"><?php echo lang('pembayaran') ?></label>
                            <select class="form-control m-bot15" name="pembayaran" value='' placeholder="">
                                <option value="Asuransi/Jaminan Kesehatan">Asuransi/Jaminan Kesehatan</option>
                                <option value="Umum/Mandiri">Umum/Mandiri</option> 
                            </select>
                        </div>
                    
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('pnj'); ?></label>
                        <input type="text" class="form-control" name="pnj" id="exampleInputEmail1" value='' placeholder="">
                    </div>

                     <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('phone'); ?></label>
                        <input type="text" class="form-control" name="phone2" id="exampleInputEmail1" value='' placeholder="">
                    </div>

                    

                    <!-- <div class="form-group col-md-6">    
                        <label for="exampleInputEmail1"><?php echo lang('doctor'); ?></label>
                        <select class="form-control js-example-basic-single"  name="doctor" value=''> 
                            <option value=""> </option>
                            <?php foreach ($doctors as $doctor) { ?>                                        
                                <option value="<?php echo $doctor->id; ?>"><?php echo $doctor->name; ?> </option>
                            <?php } ?> 
                        </select>
                    </div> -->



                    <div class="form-group last col-md-6">
                        <label class="control-label">Image Upload</label>
                        <div class="">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                    <img src="//www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                <div>
                                    <span class="btn btn-white btn-file">
                                        <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                        <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                        <input type="file" class="default" name="img_url"/>
                                    </span>
                                    <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
                                </div>
                            </div>

                        </div>
                    </div>

                    <!--
                                        <div class="form-group last col-md-6">
                                            <div style="text-align:center;" class="col-md-12">
                                                <video id="video" width="200" height="200" autoplay></video>
                                                <div class="snap" id="snap">Capture Photo</div>
                                                <canvas id="canvas" width="200" height="200"></canvas>
                                                Right click on the captured image and save. Then select the saved image from the left side's Select Image button.
                                            </div>
                                        </div>
                    -->


                    <div class="form-group col-md-6">
                        <input type="checkbox" name="sms" value="sms"> <?php echo lang('send_sms') ?><br>
                    </div>


                    <section class="col-md-12">
                        <button type="submit" name="submit" class="btn btn-info pull-right"><?php echo lang('submit'); ?></button>
                    </section>
                </form>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- Add Patient Modal-->







<!-- Edit Patient Modal-->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">  <?php echo lang('edit_patient'); ?></h4>
            </div>
            <div class="modal-body row">
                <form role="form" id="editPatientForm" action="patient/addNew" class="clearfix" method="post" enctype="multipart/form-data">

                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('no_rm'); ?></label>
                        <input type="text" class="form-control" name="no_rm" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('name'); ?></label>
                        <input type="text" class="form-control" name="name" id="exampleInputEmail1" value='' placeholder="">
                    </div>

                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('nik'); ?></label>
                        <input type="text" class="form-control" name="nik" id="exampleInputEmail1" value='' placeholder="">
                    </div>

                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('email'); ?></label>
                        <input type="text" class="form-control" name="email" id="exampleInputEmail1" value='' placeholder="">
                    </div>

                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('password'); ?></label>
                        <input type="password" class="form-control" name="password" id="exampleInputEmail1" placeholder="">
                    </div>

                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('phone'); ?></label>
                        <input type="text" class="form-control" name="phone" id="exampleInputEmail1" value='' placeholder="">
                    </div>

                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('place_of_birth'); ?></label>
                        <input type="text" class="form-control" name="placeofbirth" id="exampleInputEmail1" value='' placeholder="">
                    </div>

                    <div class="form-group col-md-6">
                        <label><?php echo lang('birth_date'); ?></label>
                        <input class="form-control form-control-inline input-medium default-date-picker" type="text" name="birthdate" value="" placeholder="" readonly="">      
                    </div>

                     <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('sex'); ?></label>
                        <select class="form-control m-bot15" name="sex" value=''>

                            <option value="Laki - Laki" <?php
                            if (!empty($patient->sex)) {
                                if ($patient->sex == 'Laki - Laki') {
                                    echo 'selected';
                                }
                            }
                            ?> > Laki - Laki </option>
                            <option value="Perempuan" <?php
                            if (!empty($patient->sex)) {
                                if ($patient->sex == 'Perempuan') {
                                    echo 'selected';
                                }
                            }
                            ?> > Perempuan </option>
                            <option value="Lainnya" <?php
                            if (!empty($patient->sex)) {
                                if ($patient->sex == 'Lainnya') {
                                    echo 'selected';
                                }
                            }
                            ?> > Lainnya </option>
                        </select>
                    </div>

                     <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('blood_group'); ?></label>
                        <select class="form-control m-bot15" name="bloodgroup" value=''>
                            <?php foreach ($groups as $group) { ?>
                                <option value="<?php echo $group->group; ?>" <?php
                                if (!empty($patient->bloodgroup)) {
                                    if ($group->group == $patient->bloodgroup) {
                                        echo 'selected';
                                    }
                                }
                                ?> > <?php echo $group->group; ?> </option>
                                    <?php } ?> 
                        </select>
                    </div>

                     <div class="form-group col-md-6">
                            <label for="exampleInputEmail1"><?php echo lang('pendidikan') ?></label>
                            <select class="form-control m-bot15" name="lasteducation" value='' placeholder="">
                                <option value="SD">SD</option>
                                <option value="SMP">SMP</option> 
                                <option value="SMA">SMA</option> 
                                <option value="DIPLOMA">Diploma</option>  
                                <option value="S1">S1</option> 
                                <option value="S2">S2</option> 
                                <option value="S3">S3</option> 
                            </select>
                        </div>

                    <div class="form-group col-md-6">
                            <label for="exampleInputEmail1"><?php echo lang('nationality') ?></label>
                            <select class="form-control m-bot15" name="nationality" value='' placeholder="">
                                <option value="WNI">WNI</option>
                                <option value="WNA">WNA</option> 
                            </select>
                        </div>
                    
                    <div class="form-group col-md-6">
                            <label for="exampleInputEmail1"><?php echo lang('religion') ?></label>
                            <select class="form-control m-bot15" name="religion" value='' placeholder="">
                                <option value="Islam">Islam</option>
                                <option value="Kristen">Kristen</option> 
                                <option value="Katholik">Katholik</option> 
                                <option value="Hindu">Hindu</option>  
                                <option value="Budha">Budha</option> 
                                <option value="Lain - Lain">Lain - Lain</option> 
                            </select>
                        </div>
                
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('address'); ?></label>
                        <input type="text" class="form-control" name="address" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    
                    
                     <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('province') ?></label>
                        <select class="form-control m-bot15" name="province" value='' placeholder="" onchange="getKabupaten(this.value)">
                            <?php foreach ($province as $p) { ?>
                                <option value="<?php echo $p->id; ?>" <?php
                                if (!empty($detailaddress->kpu_idprov)) {
                                    if ($p->id == $detailaddress->kpu_idprov) {
                                        echo 'selected';
                                    }
                                }
                                ?>> <?php echo $p->provinsi; ?> </option>
                                    <?php } ?>
                        </select>
                    </div>

                    <div id="kab">
                        <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('regency') ?></label>
                        <select class="form-control m-bot15" name="regency" value='' placeholder="" onchange="getKecamatan(this.value)">
                            <?php foreach ($regency as $r) { ?>
                                <option value="<?php echo $r->id; ?>" <?php
                                if (!empty($detailaddress->kpu_idkab)) {
                                    if ($r->id == $detailaddress->kpu_idkab) {
                                        echo 'selected';
                                    }
                                }
                                ?>> <?php echo $r->kabkot; ?> </option>
                                    <?php } ?>
                        </select>
                    </div>
                    </div> 

                    <div id="kec">
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1"><?php echo lang('sub_district') ?></label>
                            <select class="form-control m-bot15" name="sub_district" value='' placeholder="" onchange="getDesa(this.value)">
                                <?php foreach ($sub_district as $s) { ?>
                                    <option value="<?php echo $s->id; ?>" <?php
                                    if (!empty($detailaddress->kpu_idkec)) {
                                        if ($s->id == $detailaddress->kpu_idkec) {
                                            echo 'selected';
                                        }
                                    }
                                    ?>> <?php echo $s->kecamatan; ?> </option>
                                        <?php } ?>
                            </select>
                        </div>
                    </div> 

                     <div id="desa">
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1"><?php echo lang('village') ?></label>
                            <select class="form-control m-bot15" id="village" name="village" value='' placeholder="">
                                
                                <?php if(isset($village)){
                                    foreach ($village as $v) { ?>                                        
                                        <option value="<?php echo $v->id; ?>"
                                        <?php
                                        if (!empty($patient->id_desa)) {
                                            if ($v->id_desa == $patient->id_desa) {
                                                echo 'selected';
                                            }
                                        }?>>
                                            <?php echo $v->desa; ?> </option>
                                <?php } 
                                } ?>   
                            </select>
                        </div>
                    </div> 

                    
                    
                    <div class="form-group col-md-6">
                            <label for="exampleInputEmail1"><?php echo lang('profession') ?></label>
                            <select class="form-control m-bot15" name="profession" value='' placeholder="">
                                <option value="PNS">PNS</option>
                                <option value="TNI/POLRI">TNI/POLRI</option> 
                                <option value="Wiraswasta">Wiraswasta</option> 
                                <option value="Pelajar/Mahasiswa">Pelajar/Mahasiswa</option>  
                                <option value="Swasta">Swasta</option> 
                                <option value="Lain - Lain">Lain - Lain</option> 
                            </select>
                        </div>

                    
                    <div class="form-group col-md-6">
                            <label for="exampleInputEmail1"><?php echo lang('status') ?></label>
                            <select class="form-control m-bot15" name="status" value='' placeholder="">
                                <option value="Belum Menikah">Belum Menikah</option>
                                <option value="Menikah">Menikah</option> 
                                <option value="Janda">Janda</option> 
                                <option value="Duda">Duda</option>  
                            </select>
                        </div>

                    <div class="form-group col-md-6">
                            <label for="exampleInputEmail1"><?php echo lang('kel_pasien') ?></label>
                            <select class="form-control m-bot15" name="kel_pasien" value='' placeholder="">
                                <option value="Umum">Umum</option>
                                <option value="Mahasiswa UMS">Mahasiswa UMS</option> 
                                <option value="Karyawan/Dosen/Rektor UMS">Karyawan/Dosen/Rektor UMS</option> 
                                <option value="Karyawan RSGM Soelastri">Karyawan RSGM Soelastri</option>  
                            </select>
                        </div>

                    <div class="form-group col-md-6">
                            <label for="exampleInputEmail1"><?php echo lang('pembayaran') ?></label>
                            <select class="form-control m-bot15" name="pembayaran" value='' placeholder="">
                                <option value="Asuransi/Jaminan Kesehatan">Asuransi/Jaminan Kesehatan</option>
                                <option value="Umum/Mandiri">Umum/Mandiri</option> 
                            </select>
                        </div>
                    
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('pnj'); ?></label>
                        <input type="text" class="form-control" name="pnj" id="exampleInputEmail1" value='' placeholder="">
                    </div>

                     <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('phone'); ?></label>
                        <input type="text" class="form-control" name="phone2" id="exampleInputEmail1" value='' placeholder="">
                    </div>

                    <div class="form-group col-md-6"></div>
                    <div class="form-group last col-md-6">
                        <label class="control-label">Image Upload</label>
                        <div class="">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                    <img src="//www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" id="img" alt="" />
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                <div>
                                    <span class="btn btn-white btn-file">
                                        <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                        <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                        <input type="file" class="default" name="img_url"/>
                                    </span>
                                    <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
                                </div>
                            </div>

                        </div>
                    </div>

                    <!--
                    
                    <div class="form-group last col-md-6">
                        <div style="text-align:center;">
                            <video id="video" width="200" height="200" autoplay></video>
                            <div class="snap" id="snap">Capture Photo</div>
                            <canvas id="canvas" width="200" height="200"></canvas>
                            Right click on the captured image and save. Then select the saved image from the left side's Select Image button.
                        </div>
                    </div>
                    
                    -->

                    <div class="form-group col-md-6">
                        <input type="checkbox" name="sms" value="sms"> <?php echo lang('send_sms') ?><br>
                    </div>

                    <input type="hidden" name="id" value=''>
                    <input type="hidden" name="p_id" value='<?php
                    if (!empty($patient->patient_id)) {
                        echo $patient->patient_id;
                    }
                    ?>'>

                    <section class="col-md-12">
                        <button type="submit" name="submit" class="btn btn-info pull-right"><?php echo lang('submit'); ?></button>
                    </section>

                </form>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
</div>
<!-- Edit Patient Modal-->



<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg"> 
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">  <?php echo lang('patient'); ?>  <?php echo lang('info'); ?></h4>
            </div>
            <div class="modal-body row">
                <form role="form" id="editPatientForm" action="patient/addNew" class="clearfix" method="post" enctype="multipart/form-data">

                    <div class="form-group last col-md-4">
                        <div class="">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                    <img src="//www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" id="img1" alt="" />
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                            </div>
                            <div class="col-md-12">
                                <label for="exampleInputEmail1"><?php echo lang('patient_id'); ?>: <span class="patientIdClass"></span></label>
                            </div>
                        </div>

                    </div>
                    <div class="form-group col-md-4">
                        <label for="exampleInputEmail1"><?php echo lang('name'); ?></label>
                        <div class="nameClass"></div>
                    </div>


                    <div class="form-group col-md-4">
                        <label for="exampleInputEmail1"><?php echo lang('email'); ?></label>
                        <div class="emailClass"></div>
                    </div>

                    <div class="form-group col-md-4">
                        <label><?php echo lang('age'); ?></label>
                        <div class="ageClass"></div>     
                    </div>

                    <div class="form-group col-md-4">
                        <label for="exampleInputEmail1"><?php echo lang('address'); ?></label>
                        <div class="addressClass"></div>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="exampleInputEmail1"><?php echo lang('gender'); ?></label>
                        <div class="genderClass"></div>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="exampleInputEmail1"><?php echo lang('phone'); ?></label>
                        <div class="phoneClass"></div>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="exampleInputEmail1"><?php echo lang('blood_group'); ?></label>
                        <div class="bloodgroupClass"></div>
                    </div>

                    <div class="form-group col-md-4">
                        <label><?php echo lang('birth_date'); ?></label>
                        <div class="birthdateClass"></div>     
                    </div>
                    <div class="form-group col-md-4">    
                    </div>
                    <div class="form-group col-md-4">    
                    </div>
                    <div class="form-group col-md-4">    
                        <label for="exampleInputEmail1"><?php echo lang('doctor'); ?></label>
                        <div class="doctorClass"></div>
                    </div>
                </form>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
</div>

<div class="modal fade" id="myModal5" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content" id="modalContent">
            
        </div>
    </div>
</div>


<script src="common/js/codearistos.min.js"></script>

<!--
<script>


    var video = document.getElementById('video');
    // Get access to the camera!
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        // Not adding `{ audio: true }` since we only want video now
        navigator.mediaDevices.getUserMedia({video: true}).then(function (stream) {
            video.src = window.URL.createObjectURL(stream);
            video.play();
        });
    }

    // Elements for taking the snapshot
    var canvas = document.getElementById('canvas');
    var context = canvas.getContext('2d');
    var video = document.getElementById('video');
    // Trigger photo take
    document.getElementById("snap").addEventListener("click", function () {
        context.drawImage(video, 0, 0, 200, 200);
    });

</script>

-->



<script type="text/javascript">

    $(".table").on("click", ".editbutton", function () {
        //    e.preventDefault(e);
        // Get the record's ID via attribute  
        var iid = $(this).attr('data-id');
        $("#img").attr("src", "uploads/cardiology-patient-icon-vector-6244713.jpg");
        $('#editPatientForm').trigger("reset");
        $.ajax({
            url: 'patient/editPatientByJason?id=' + iid,
            method: 'GET',
            data: '',
            dataType: 'json',
        }).success(function (response) {
            // Populate the form fields with the data returned from server

            $('#editPatientForm').find('[name="id"]').val(response.patient.id).end()
            $('#editPatientForm').find('[name="name"]').val(response.patient.name).end()
            $('#editPatientForm').find('[name="password"]').val(response.patient.password).end()
            $('#editPatientForm').find('[name="email"]').val(response.patient.email).end()
            $('#editPatientForm').find('[name="address"]').val(response.patient.address).end()
            $('#editPatientForm').find('[name="phone"]').val(response.patient.phone).end()
            $('#editPatientForm').find('[name="sex"]').val(response.patient.sex).end()
            $('#editPatientForm').find('[name="birthdate"]').val(response.patient.birthdate).end()
            $('#editPatientForm').find('[name="bloodgroup"]').val(response.patient.bloodgroup).end()
            $('#editPatientForm').find('[name="p_id"]').val(response.patient.patient_id).end()
            $('#editPatientForm').find('[name="province"]').val(response.detailaddress.kpu_idprov).end()
            $('#editPatientForm').find('[name="regency"]').val(response.detailaddress.kpu_idkab).end()
            $('#editPatientForm').find('[name="sub_district"]').val(response.detailaddress.kpu_idkec).end()

            $('#editPatientForm').find('[name="no_rm"]').val(response.patient.no_rm).end()
            $('#editPatientForm').find('[name="nik"]').val(response.patient.nik).end()
            $('#editPatientForm').find('[name="placeofbirth"]').val(response.patient.placeofbirth).end()
            $('#editPatientForm').find('[name="nationality"]').val(response.patient.nationality).end()
            $('#editPatientForm').find('[name="pendidikan"]').val(response.patient.lasteducation).end()
            $('#editPatientForm').find('[name="religion"]').val(response.patient.religion).end()
            $('#editPatientForm').find('[name="profession"]').val(response.patient.profession).end()
            $('#editPatientForm').find('[name="status"]').val(response.patient.status).end()
            $('#editPatientForm').find('[name="kel_pasien"]').val(response.patient.kel_pasien).end()
            $('#editPatientForm').find('[name="pembayaran"]').val(response.patient.pembayaran).end()
            $('#editPatientForm').find('[name="pnj"]').val(response.patient.penanggungjawab).end()
            $('#editPatientForm').find('[name="phone2"]').val(response.patient.phonepnj).end()
            

            
            desa = '<option value='+response.detailaddress.id_desa+'>'+response.detailaddress.desa+'</option>';
            $('#editPatientForm').find('[name="village"]').html(desa);
            

            if (typeof response.patient.img_url !== 'undefined' && response.patient.img_url != '') {
                $("#img").attr("src", response.patient.img_url);
            }


            $('.js-example-basic-single.doctor').val(response.patient.doctor).trigger('change');

            $('#myModal2').modal('show');

        });
    });

</script>



<script type="text/javascript">

    $(".table").on("click", ".inffo", function () {
        //    e.preventDefault(e);
        // Get the record's ID via attribute  
        var iid = $(this).attr('data-id');
        
        $("#img1").attr("src", "uploads/cardiology-patient-icon-vector-6244713.jpg");
        $('.patientIdClass').html("").end()
        $('.nameClass').html("").end()
        $('.emailClass').html("").end()
        $('.addressClass').html("").end()
        $('.phoneClass').html("").end()
        $('.genderClass').html("").end()
        $('.birthdateClass').html("").end()
        $('.bloodgroupClass').html("").end()
        $('.patientidClass').html("").end()
        $('.doctorClass').html("").end()
        $('.ageClass').html("").end()
        $.ajax({
            url: 'patient/getPatientByJason?id=' + iid,
            method: 'GET', 
            data: '',
            dataType: 'json', 
        }).success(function (response) {

           
            // Populate the form fields with the data returned from server

            $('.patientIdClass').append(response.patient.id).end()
            $('.nameClass').append(response.patient.name).end()
            $('.emailClass').append(response.patient.email).end()
            $('.addressClass').append(response.patient.address).end()
            $('.phoneClass').append(response.patient.phone).end()
            $('.genderClass').append(response.patient.sex).end()
            $('.birthdateClass').append(response.patient.birthdate).end()
            $('.ageClass').append(response.age).end()
            $('.bloodgroupClass').append(response.patient.bloodgroup).end()
            $('.patientidClass').append(response.patient.patient_id).end()
            $('.doctorClass').append(response.doctor.name).end()

            if (typeof response.patient.img_url !== 'undefined' && response.patient.img_url != '') {
                $("#img1").attr("src", response.patient.img_url);
            }


            $('#infoModal').modal('show');

        });
    });

</script>

<script>


    $(document).ready(function () {
        var table = $('#editable-sample').DataTable({
            responsive: true,
            //   dom: 'lfrBtip',

            "processing": true,
            "serverSide": true,
            "searchable": true,
            "ajax": {
                url: "patient/getPatient",
                type: 'POST',
            },
            scroller: {
                loadingIndicator: true
            },
            dom: "<'row'<'col-sm-3'l><'col-sm-5 text-center'B><'col-sm-4'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5',
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [0, 1, 2],
                    }
                },
            ],
            aLengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "All"]
            ],
            iDisplayLength: 100,
            "order": [[0, "desc"]],

            "language": {
                "lengthMenu": "_MENU_",
                search: "_INPUT_",
                "url": "common/assets/DataTables/languages/<?php echo $this->language; ?>.json"
            }
        });
        table.buttons().container().appendTo('.custom_buttons');
    });

</script>



<script>
    $(document).ready(function () {
        $(".flashmessage").delay(3000).fadeOut(100);
    });
    function MBarcode(ID){
             $.ajax({
				url:'patient/getBarcode?id=' + ID,
				success:function(response){	
	                $("#modalContent").html(response);
				}
			});
         }
     function BuktiBayarPendf(ID){
             $.ajax({
				url:'patient/getBuktiBayar?id=' + ID,
				success:function(response){	
	                $("#modalContent").html(response);
				}
			});
    }
    function getKabupaten(ID){
        $.ajax({
            url: 'patient/getRegency?id=' + ID,
            method: 'GET', 
            data: '', 
        }).success(function (response) {
           
            $('#kab').html(response);

        });
    }
    function getKecamatan(ID){
         $.ajax({
            url: 'patient/getKecamatan?id=' + ID,
            method: 'GET', 
            data: '', 
        }).success(function (response) {
            $('#kec').html(response);

        });
    }
    function getDesa(ID){
        $.ajax({
            url: 'patient/getDesa?id=' + ID,
            method: 'GET', 
            data: '', 
        }).success(function (response) {
            $('#desa').html(response);

        });
    }

    function printCard(ID){
        $.ajax({
				url:'patient/printCard?id=' + ID,
				success:function(response){	
	                $("#modalContent").html(response);
				}
			});
    }

   
</script>



