<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Settings_model extends CI_model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function getSettings() {
        $query = $this->db->get('settings');
        return $query->row();
    }

    function updateSettings($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('settings', $data);
    }

    function getRegristrationfee(){
        $this->db->where('iscurrent', '1');
        $query = $this->db->get('setting_biaya');
        return $query->row();
    }
    function updateRegristrationfee() {
        $this->db->where('iscurrent', '1');
        $this->db->update('setting_biaya', array('iscurrent' => '0'));
    }
    
    function insertRegristrationFee($data) {
        $this->db->insert('setting_biaya', $data);
    }

}
