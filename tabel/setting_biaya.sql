/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 100132
 Source Host           : localhost:3306
 Source Schema         : rsgmsoelastri

 Target Server Type    : MySQL
 Target Server Version : 100132
 File Encoding         : 65001

 Date: 01/03/2020 18:14:34
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for setting_biaya
-- ----------------------------
DROP TABLE IF EXISTS `setting_biaya`;
CREATE TABLE `setting_biaya`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `biaya` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `iscurrent` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of setting_biaya
-- ----------------------------
INSERT INTO `setting_biaya` VALUES (1, '45000', '0');
INSERT INTO `setting_biaya` VALUES (2, '25000', '0');
INSERT INTO `setting_biaya` VALUES (3, '50000', '0');
INSERT INTO `setting_biaya` VALUES (4, '25000', '0');
INSERT INTO `setting_biaya` VALUES (5, '25000', '1');

SET FOREIGN_KEY_CHECKS = 1;
